#ovladani gripru je od 0 az 0.064, 0.064 = open
# pri open ma griper rozpeti cca 65 mm vcetne molitanu, 62 mm je ciste rozpeti
# stredni krychle - strana = 55 mm, tj 85 % gripru, optimalni sevreni je 0.056


# idealni hladina nad stolem je x,y,z = 0, 0.8, 0.09, tmp_pos = np.array([[0], [0.8], [0.09], [0], [pi], [pi/2]])
# kostky poustet na hladine 0.095

from __future__ import division
# main rospy package
import rospy

import cv2

from sensor_msgs.msg import Image

from cv_bridge import CvBridge

img_name = 'MY IMG'
bridge = CvBridge()


def my_callback(data):
    #print("Callback")
    img = bridge.imgmsg_to_cv2(data, "bgr8")
    cv2.imshow("Kalib", img)
    cv2.waitKey(3)



if __name__ == '__main__':
	rospy.init_node('zidan')
	rospy.Subscriber("/camera/image_color", Image, my_callback, queue_size=1)
	rospy.spin()
    
